#Glosario

- Request: Un mensaje request incluye, en la primera linea del mensaje, el metodo a ser alicado al recurso, el identificador del recurso, y el protocolo de la version en uso.
- Response: El servidor responde con un mensaje de respuesta http luego de recibir y interpretar un request.
- Status codes: Este elemento es un codigo entero de 3 digitos resultado de los intentos de comprender y satisfacer el request.
- Methods: Se indica el metodo a ser realizado on el recurso identificado por el request.
    - GET:Este sirve para pedir la representacion del recurso especificado.
    - POST: Este sirve para anviar los datos a procesar a un recurso especificado
    - HEAD: Es igual a Get pero devuelve solo encabezados HTTP 
    - OPTIONS: Devuelve metos HTTP que admite el servidor
    - PUT: Carga una representacion del URI especificado
    - DELETE: Elimina el recurso especificado
- Header: Un grupo de ayudas introductorias o de navegacion.
    - Accept, Accept-Charset, Accept-Encoding:
    - Cache Control:
    - Connection:
    - Cookie, Set-Cookie:
    - Host:
    - Origin:
    - Referer:
    - User-Agent:
    - Content-Encoding, Content-Lenght, Content-Type:
    - Location:
    - Upgrade: 
    




|**REQ/RES**|**Método HTTP**|**URL**|**Headers**|**Status**|**Descripcion**|  
|----------|:-------------------:|-------------------------------|-----------------------------------------|:--------------:|------------------------|
|REQ|GET|http://www.inf.ucv.cl/~ifigueroa|Upgrade-Insecure-Requests, User-Agent, Accept, Accept-Encoding, Accept-Language y  Cookie.| n/a| Solicitud inicial del sitio web.|
|RES|n/a|n/a|Redirect to, Date,Server,Location,Content-Length,Connection,Content-Type.| 301 | La redireccion permite la consolidación de la pagina.|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa  |Upgrade-Insecure-Requests,User-Agent,Accept,Accept-Encoding,Accept-Language,Cookie.|n/a|Petición inicial del sitio web|
|RES|n/a|n/a|Redirect to, Date, Server, Location, Vary, Content-Encoding, Content-Length,Keep-Alive, Connection, Content-Type.| 301| |
|REQ|GET                        |http://zeus.inf.ucv.cl/~ifigueroa/|Upgrade-Insecure-Requests, User-Agent, Accept, Accept-Encoding, Accept-Language, Cookie| n/a| Solicitud inicial del sitio web.|
|RES|n/a|n/a|Redirect to, Date, Server, X-Powered-By, Location, Vary, Content-Encoding, Content-Length,Keep-Alive, Connection, Content-Type.| 302|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/doku.php|Upgrade-Insecure-Requests, User-Agent, Accept, Accept-Encoding, Accept-Language Y Cookie.|n/a| La Solicitud inicial del sitio web.|
|RES|n/a|n/a|Date, Server, X-Powered-By, Expires, Cache-Control, Pragma, X-UA-Compatible, Set-Cookie, Vary, Content-Encoding, Content-Length, Keep-Alive, Connection y Content-Type.	|200|	|

* 5) Como podemos apreciar en esta tabla, las distintas url van pidiendo la consolidacion del sitio a traves de sus respectivas peticiones hasta que esta solicitud de respuesta de exito.



.



| REQ/RES|Método HTTP(solo REQ)|URL| Headers | Status (solo RES) | Descripción|
| :-------:| :------: | :-----: |  ---         |  :-----:| :----:|
|  REQ | GET | http://www.wuxiaworld.com/| Upgrade-Insecure-Requests,User-Agent,Accept,Accept-Encoding,Accept-Language,Cookie| n/a|Solicitud inicial del sitio web|
|  RES | n/a| n/a |Date,Content-Type,Transfer-Encoding,Connection,Vary,Link,Content-Security,X-Frame-Options,X-Content-Type-Option,X-XSS-Protection,X-Varnish,Age,Via,X-Cache,X-Cache-Hits,Server,CF-RAY,Content-Encoding| 200|Recibió bien la solicitud y da la respuesta de éxito.|
| REQ| GET | https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css| User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie | n/a| Solicitud inicial del sitio web |
| RES| n/a | n/a|status,date,content-type,last-modified,etag,server,expires,cache.control,vary,access-control,x-hello-human,x-cache,content-encoding | 200|  |
| REQ| GET |http://www.wuxiaworld.com/wp-content/themes/wuxia-world/style-nightmode.css?v=4| User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie| n/a|Solicitud inicial del sitio web |
| RES| n/a | n/a|Date,Content-Type,Last-Modified,Etag,Expires,Cache-Control,Content-Security-Policy,X-Frame-Options,X-Content-Type-Options,X-XSS-Protection,Vary,X-Varnish,Via,X-Cache,X-Cache-Hits,CF-Cache-Status,Server,CF-RAY,Content-Encoding| 200| 	La redireccion permite la consolidación de la pagina. |
| REQ| GET |http://www.wuxiaworld.com/wp-content/themes/wuxia-world/js/libs/device-detection.js?v=1| User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie| n/a| Solicitud inicial del sitio web |
| RES| n/a | n/a|Date,Content-Type,Last-Modified,Etag,Expires,Cache-Control,Content-Security-Policy,X-Frame-Options,X-Content-Type-Options,X-XSS-Protection,Vary,X-Varnish,Via,X-Cache,X-Cache-Hits,CF-Cache-Status,Server,CF-RAY,Content-Encoding | 200| 	La redireccion permite la consolidación de la pagina. |
| REQ| GET |http://www.wuxiaworld.com/wp-content/themes/wuxia-world/js/libs/storage.js?v=2| User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie| n/a| Solicitud inicial del sitio web|
| RES| n/a | n/a|Date,Content-Type,Last-Modified,Etag,Expires,Cache-Control,Content-Security-Policy,X-Frame-Options,X-Content-Type-Options,X-XSS-Protection,Vary,X-Varnish,Via,X-Cache,X-Cache-Hits,CF-Cache-Status,Server,CF-RAY,Content-Encoding | 200| 	La redireccion permite la consolidación de la pagina. |

* 